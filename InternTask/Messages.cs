namespace InternTask{
    public class Messages : IMessages{
       public void MakingCoffee(Coffee chosenCoffee){
            Console.WriteLine("Fetching cup....");
            Console.WriteLine("Making " + chosenCoffee.name);
            Console.WriteLine("Adding sugar (level: " + chosenCoffee.sugar + ")");
        }
       public void PreparingJuice(Juice chosenJuice){
        Console.WriteLine("Fetching cup....");
        Console.WriteLine("Preparing " + chosenJuice.name);
       }
       public void InvalidInput(){
        Console.WriteLine("Invalid input");
       }
       public void Enjoy(){
        Preparing();
        Console.WriteLine("Enjoy");
       }

       public void PickADrink(){
        Console.WriteLine("Pick a drink");
       }

       private void Preparing(){
        for(int counter = 0; counter < 10; counter++){
            Console.WriteLine(".");
            System.Threading.Thread.Sleep(1000);
        }
       }
    }
}
        