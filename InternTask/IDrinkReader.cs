namespace InternTask{
    public interface IDrinkReader{
        public void DrinkDetails(List<Drink> drinkList);
    }
}