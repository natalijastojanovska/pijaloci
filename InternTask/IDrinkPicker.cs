namespace InternTask{
    public interface IDrinkPicker{
        public bool PickDrink(int id, List<Drink> drinkList);
    }
}