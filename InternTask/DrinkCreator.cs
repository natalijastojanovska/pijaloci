namespace InternTask{
    public class DrinkCreator : IDrinkCreator{
        public List<Drink> CreateNewDrinks(){ 
            List<Drink> drinkList = new List<Drink>();

            Drink latte = new Coffee(1, "latte", 35);
            drinkList.Add(latte);
            Drink orangeJuice = new Juice(2, "orange juice", 55);
            drinkList.Add(orangeJuice);
            Drink appleJuice = new Juice(3, "apple juice", 75);
            drinkList.Add(appleJuice);

            return drinkList;
        }
    }
}