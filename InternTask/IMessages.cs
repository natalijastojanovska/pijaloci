namespace InternTask{
    public interface IMessages{
       public void MakingCoffee(Coffee chosenCoffee);
       public void PreparingJuice(Juice chosenJuice);
       public void InvalidInput();
       public void Enjoy();
       public void PickADrink();
    }
}