namespace InternTask{
    public class DrinkPicker : IDrinkPicker{
        public bool PickDrink(int id, List<Drink> drinkList){
            IMessages messages = new Messages();
            bool found = false;
            foreach (Drink drink in drinkList)
            {
                if(drink.id == id){
                    if(drink.GetType() == typeof(Coffee)){
                        Coffee chosenCoffee = (Coffee) drink;
                        Console.WriteLine("Choose level of sugar: ");
                        int sugarLevel = Convert.ToInt32(Console.ReadLine());
                        if(!(sugarLevel >= 0 && sugarLevel <= 3)){
                            messages.InvalidInput();
                            return false;
                        }
                        chosenCoffee.SetSugarLevel(sugarLevel);
                        messages.MakingCoffee(chosenCoffee);

                    }
                    else{
                        messages.PreparingJuice((Juice) drink);
                    }
                    messages.Enjoy();
                    found = true;
                    break;
                }
            }
            if(!found){
                messages.InvalidInput();
                return false;
            }

            return true;
        }
    }
}