namespace InternTask{
    public class Coffee : Drink{
        public enum sugarLevel{
            Sugarless, // 0
            Low, // 1
            Medium, // 2
            High // 3
        }

        public sugarLevel sugar {get; set;}

        public Coffee(int id, string name, float price){
            this.id = id;
            this.name = name;
            this.price = price;
            this.sugar = (sugarLevel) 1;
        }

        public void SetSugarLevel(int sugarLevel){
            this.sugar = (sugarLevel) sugarLevel;
        }
    }
}