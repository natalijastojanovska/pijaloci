namespace InternTask{
    public class DrinkReader : IDrinkReader{
        public void DrinkDetails(List<Drink> drinkList){
            drinkList.Sort((x, y) => string.Compare(x.name, y.name));
            foreach (Drink drink in drinkList)
            {
                Console.WriteLine(drink.name + " (" + drink.price + ") ...... " + drink.id);
            }
        }
    }
}