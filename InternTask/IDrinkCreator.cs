namespace InternTask{
    public interface IDrinkCreator{
        public List<Drink> CreateNewDrinks();
    }
}