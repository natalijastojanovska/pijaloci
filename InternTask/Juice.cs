namespace InternTask{
    public class Juice : Drink{
        public Juice(int id, string name, float price){
            this.id = id;
            this.name = name;
            this.price = price;
        }
    }
}