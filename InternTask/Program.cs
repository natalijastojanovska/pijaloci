﻿namespace InternTask
{
    class Program{
        static void Main(string[] args){
           IDrinkCreator drinkCreator = new DrinkCreator();
           IDrinkReader drinkReader = new DrinkReader();
           IDrinkPicker drinkPicker = new DrinkPicker();
           IMessages messages = new Messages();


           List<Drink> drinkList = drinkCreator.CreateNewDrinks();
           drinkReader.DrinkDetails(drinkList);

           bool running = true;

           while(running){
            try{
                messages.PickADrink();
                running = !drinkPicker.PickDrink(Convert.ToInt16(Console.ReadLine()), drinkList);
            }catch{
                messages.InvalidInput();
            }
           }
            
        }
    }
}
